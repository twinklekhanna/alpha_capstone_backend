﻿using Customer.WebService.Handlers;
using Customer.WebService.Models;
using Customer.WebService.Queries;
using Customer.WebService.Services;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Customer.WebService.Test.Queries
{
    public class GetCustomerByIdRequestHandlerTest
    {
        private readonly Mock<ICustomerService> _mockRepo;

        public GetCustomerByIdRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCustomerService();
        }

        [Fact]
        public async Task GetCustomerByIdTest()
        {
            var handler = new GetByLoginIdHandler(_mockRepo.Object);

            var result = handler.Handle(new GetByLoginIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomCustomers>>();
        }
    }
}
