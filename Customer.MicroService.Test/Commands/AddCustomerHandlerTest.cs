﻿using Customer.WebService.Commands;
using Customer.WebService.Handlers;
using Customer.WebService.Models;
using Customer.WebService.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Customer.WebService.Test.Commands
{
    public class AddCustomerHandlerTest
    {
        private readonly Mock<ICustomerService> _mockRepo;
        private readonly EcomCustomers _customer;

        public AddCustomerHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCustomerService();

            _customer = new EcomCustomers
            {
                CustomerId = 3,
                CustomerName = "Vishal",
                CustomerAddress = "Banglore",
                CustomerPhoneNumber = "1234567890",
                CustomerEmailId = "vishal@gmail.com",
                LoginId = 103
            };
        }

        [Fact]
        public async Task AddCustomerTest()
        {
            var handler = new AddCustomerHandler(_mockRepo.Object);

            var result = handler.Handle(new AddCustomerCommand(), CancellationToken.None);
        }
    }
}
