﻿using Category.MicroService.Models;
using Category.MicroService.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Category.MicroService.Test.Mocks
{
    public static class MockRepository
    {
        public static Mock<ICategoryService> GetCategoryService()
        {
            var catlist = new List<EcomCategory>
            {
                new EcomCategory
                {
                    CategoryId = 300,
                    CategoryName = "Dress"
                },
                new EcomCategory
                {
                    CategoryId = 301,
                    CategoryName = "Electronics"
                }
            };

            var mockrepo =  new Mock<ICategoryService>();

            mockrepo.Setup(r => r.GetAll()).ReturnsAsync(catlist);

            mockrepo.Setup(r => r.AddCategory(It.IsAny<EcomCategory>())).ReturnsAsync((EcomCategory category) =>
            {
                catlist.Add(category);
                return catlist;
            });

            return mockrepo;
        }
    }
}
