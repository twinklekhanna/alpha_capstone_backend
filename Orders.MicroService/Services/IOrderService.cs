﻿using Orders.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orders.MicroService.Services
{
    public interface IOrderService
    {
        Task<EcomOrders> AddOrder(EcomOrders order);
        Task<IEnumerable<EcomOrders>> GetAllById(int custid);
    }
}