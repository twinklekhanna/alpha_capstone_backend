﻿using MediatR;
using Orders.MicroService.Commands;
using Orders.MicroService.Models;
using Orders.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Orders.MicroService.Handlers
{
    public class AddOrderHandler : IRequestHandler<AddOrderCommand, EcomOrders>
    {
        private readonly IOrderService _orderService;

        public AddOrderHandler(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public Task<EcomOrders> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            return _orderService.AddOrder(request.Order);
        }
    }
}
