﻿using MediatR;
using Orders.MicroService.Models;
using Orders.MicroService.Queries;
using Orders.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Orders.MicroService.Handlers
{
    public class GetOrdersByIdHandler : IRequestHandler<GetOrdersByIdQuery, IEnumerable<EcomOrders>>
    {
        private readonly IOrderService _orderService;

        public GetOrdersByIdHandler(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public Task<IEnumerable<EcomOrders>> Handle(GetOrdersByIdQuery request, CancellationToken cancellationToken)
        {
            return _orderService.GetAllById(request.CustomerId);
        }
    }
}
