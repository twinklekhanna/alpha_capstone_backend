﻿using MediatR;
using Orders.MicroService.Models;
using System.Collections.Generic;

namespace Orders.MicroService.Commands
{
    public class AddOrderCommand : IRequest<EcomOrders>
    {
        public EcomOrders Order { get; set; }
    }
}
