﻿using MediatR;
using Orders.MicroService.Models;
using System.Collections.Generic;

namespace Orders.MicroService.Queries
{
    public class GetOrdersByIdQuery : IRequest<IEnumerable<EcomOrders>>
    {
        public int CustomerId { get; set; }
    }
}
