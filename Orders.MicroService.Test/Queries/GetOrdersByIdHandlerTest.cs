﻿using Moq;
using Orders.MicroService.Handlers;
using Orders.MicroService.Models;
using Orders.MicroService.Services;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Orders.MicroService.Test.Queries
{
    public class GetOrdersByIdHandlerTest
    {
        private readonly Mock<IOrderService> _mockRepo;

        public GetOrdersByIdHandlerTest()
        {
            _mockRepo = Mock.MockRepository.GetLoginService();
        }

        [Fact]
        public async Task GetLoginTest()
        {
            var handler = new GetOrdersByIdHandler(_mockRepo.Object);

            var result = await handler.Handle(new MicroService.Queries.GetOrdersByIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<IEnumerable<EcomOrders>>>();
        }
    }
}
