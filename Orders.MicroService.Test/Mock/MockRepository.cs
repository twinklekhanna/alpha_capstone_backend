﻿using Moq;
using Orders.MicroService.Models;
using Orders.MicroService.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orders.MicroService.Test.Mock
{
    public static class MockRepository
    {
        public static Mock<IOrderService> GetLoginService()
        {
            var catlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    OrderId = 717,
                    ProductId = 502,
                    CustomerId = 4,
                    OrderQuantity = 2,
                    OrderPrice = 1340,
                    ShipmentAddress = "gautam nagar bhopal"
                },
                new EcomOrders
                {
                    OrderId = 716,
                    ProductId = 504,
                    CustomerId = 4,
                    OrderQuantity = 1,
                    OrderPrice = 830,
                    ShipmentAddress = "gautam nagar bhopal"
                }
            };

            var mockrepo = new Mock<IOrderService>();

            mockrepo.Setup(r => r.GetAllById(101).Result).Returns(catlist);

            return mockrepo;
        }
    }
}
