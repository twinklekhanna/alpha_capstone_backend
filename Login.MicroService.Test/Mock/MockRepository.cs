﻿using Login.MicroService.Models;
using Login.MicroService.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Login.MicroService.Test.Mock
{
    public static class MockRepository
    {
        public static Mock<ILoginService> GetLoginService()
        {
            var catlist = new List<EcomLogin>
            {
                new EcomLogin
                {
                    LoginId = 101,
                    Password = "pass@123",
                    Token = "qwe",
                    DateTimeStamp = DateTime.Now,
                    LoginRole = "USER",
                    UserId = "123"
                },
                new EcomLogin
                {
                    LoginId = 102,
                    Password = "pass@123",
                    Token = "qwerty",
                    DateTimeStamp = DateTime.Now,
                    LoginRole = "USER",
                    UserId = "124"
                }
            };

            var mockrepo = new Mock<ILoginService>();

            mockrepo.Setup(r => r.GetByUserId("124").Result).Returns(catlist.SingleOrDefault(x => x.UserId == "124"));

            return mockrepo;
        }
    }
}
