﻿using Customer.WebService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Customer.WebService.Services
{
    public interface ICustomerService
    {
        Task<EcomCustomers> AddCustomer(EcomCustomers customer);
        Task<EcomCustomers> GetByLoginId(int id);
    }
}