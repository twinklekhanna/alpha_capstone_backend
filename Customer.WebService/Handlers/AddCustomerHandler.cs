﻿using Customer.WebService.Commands;
using Customer.WebService.Models;
using Customer.WebService.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Customer.WebService.Handlers
{
    public class AddCustomerHandler : IRequestHandler<AddCustomerCommand, EcomCustomers>
    {
        private readonly ICustomerService _customerService;

        public AddCustomerHandler(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<EcomCustomers> Handle(AddCustomerCommand request, CancellationToken cancellationToken)
        {
            return await _customerService.AddCustomer(request.Customer);
        }
    }
}
