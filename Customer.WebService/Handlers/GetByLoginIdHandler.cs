﻿using Customer.WebService.Models;
using Customer.WebService.Queries;
using Customer.WebService.Services;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Customer.WebService.Handlers
{
    public class GetByLoginIdHandler : IRequestHandler<GetByLoginIdQuery, EcomCustomers>
    {
        private readonly ICustomerService _customerService;

        public GetByLoginIdHandler(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<EcomCustomers> Handle(GetByLoginIdQuery request, CancellationToken cancellationToken)
        {
            return await _customerService.GetByLoginId(request.Id);
        }
    }
}
