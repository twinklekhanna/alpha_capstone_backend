﻿using Customer.WebService.Models;
using MediatR;
using System.Collections.Generic;

namespace Customer.WebService.Commands
{
    public class AddCustomerCommand : IRequest<EcomCustomers>
    {
        public EcomCustomers Customer { get; set; }
    }
}
