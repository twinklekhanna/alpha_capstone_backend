﻿using Customer.WebService.Commands;
using Customer.WebService.Models;
using Customer.WebService.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Customer.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ISender _sender;

        public CustomersController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet("{id}")]
        public async Task<EcomCustomers> GetByID(int id)
        {
            return await _sender.Send(new GetByLoginIdQuery { Id = id });
        }

        [HttpPost("add")]
        public async Task<EcomCustomers> AddCustomer([FromBody] EcomCustomers customer)
        {
            return await _sender.Send(new AddCustomerCommand() { Customer = customer });
        }
    }
}
