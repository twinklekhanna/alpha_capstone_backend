﻿using Category.MicroService.Models;
using MediatR;
using System.Collections.Generic;

namespace Category.MicroService.Queries
{
    public class GetCategoriesQuery : IRequest<IEnumerable<EcomCategory>>
    {
    }
}
