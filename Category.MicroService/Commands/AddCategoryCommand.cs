﻿using Category.MicroService.Models;
using MediatR;
using System.Collections.Generic;

namespace Category.MicroService.Commands
{
    public class AddCategoryCommand : IRequest<IEnumerable<EcomCategory>>
    {
        public EcomCategory Category { get; set; }
    }
}
