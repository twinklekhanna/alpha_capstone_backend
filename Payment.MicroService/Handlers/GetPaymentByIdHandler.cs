﻿using MediatR;
using Payment.MicroService.Models;
using Payment.MicroService.Queries;
using Payment.MicroService.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.MicroService.Handlers
{
    public class GetPaymentByIdHandler : IRequestHandler<GetPaymentByIdQuery, EcomPayment>
    {
        private readonly IPaymentService _paymentService;

        public GetPaymentByIdHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<EcomPayment> Handle(GetPaymentByIdQuery request, CancellationToken cancellationToken)
        {
            return await _paymentService.GetById(request.Id);
        }
    }
}
