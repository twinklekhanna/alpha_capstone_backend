﻿using MediatR;
using Payment.MicroService.Models;
using Payment.MicroService.Queries;
using Payment.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.MicroService.Handlers
{
    public class GetPaymentDetailsHandler : IRequestHandler<GetPaymentDetailsQuery, IEnumerable<EcomPayment>>
    {
        private readonly IPaymentService _paymentService;

        public GetPaymentDetailsHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<IEnumerable<EcomPayment>> Handle(GetPaymentDetailsQuery request, CancellationToken cancellationToken)
        {
            return await _paymentService.GetAll();
        }
    }
}
