﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Payment.MicroService.Commands;
using Payment.MicroService.Models;
using Payment.MicroService.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Payment.MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly ISender _sender;

        public PaymentController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IEnumerable<EcomPayment>> GetAll()
        {
            return await _sender.Send(new GetPaymentDetailsQuery());
        }

        [HttpGet("{id}")]
        public async Task<EcomPayment> GetById(int id)
        {
            return await _sender.Send(new GetPaymentByIdQuery { Id = id });
        }

        [HttpPost("add")]
        public async Task<IEnumerable<EcomPayment>> AddPayment([FromBody] EcomPayment payment)
        {
            return await _sender.Send(new AddPaymentCommand() { Payment = payment });
        }
    }
}
