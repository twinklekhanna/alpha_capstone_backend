﻿using Payment.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Payment.MicroService.Services
{
    public interface IPaymentService
    {
        Task<IEnumerable<EcomPayment>> AddPayment(EcomPayment payment);
        Task<IEnumerable<EcomPayment>> GetAll();
        Task<EcomPayment> GetById(int id);
    }
}