﻿using MediatR;
using Products.MicroService.Models;
using Products.MicroService.Queries;
using Products.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class GetProductByCategoryIdHandler : IRequestHandler<GetProductByCategoryIdQuery, IEnumerable<EcomProducts>>
    {
        private readonly IProductService service;

        public GetProductByCategoryIdHandler(IProductService productService)
        {
            service = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(GetProductByCategoryIdQuery request, CancellationToken cancellationToken)
        {
            return await service.GetProductByCategoryId(request.CategoryId);
        }
    }
}
