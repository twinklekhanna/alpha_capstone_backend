﻿using MediatR;
using Products.MicroService.Models;
using Products.MicroService.Queries;
using Products.MicroService.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, EcomProducts>
    {
        private readonly IProductService _productService;

        public GetProductByIdHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<EcomProducts> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await _productService.GetById(request.ProductId);
        }
    }
}
