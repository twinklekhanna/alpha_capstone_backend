﻿using MediatR;
using Products.MicroService.Models;
using Products.MicroService.Queries;
using Products.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class GetProductsHandler : IRequestHandler<GetProductsQuery, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public GetProductsHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            return await _productService.GetAllProducts();
        }
    }
}
