﻿using MediatR;
using Products.MicroService.Commands;
using Products.MicroService.Models;
using Products.MicroService.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class DeleteProductHandler : IRequestHandler<DeleteProductCommand, EcomProducts>
    {
        private readonly IProductService _productService;

        public DeleteProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<EcomProducts> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.DeleteProductById(request.ProductId);
        }
    }
}
