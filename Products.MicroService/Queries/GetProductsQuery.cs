﻿using MediatR;
using Products.MicroService.Models;
using System.Collections.Generic;

namespace Products.MicroService.Queries
{
    public class GetProductsQuery : IRequest<IEnumerable<EcomProducts>>
    {
    }
}
