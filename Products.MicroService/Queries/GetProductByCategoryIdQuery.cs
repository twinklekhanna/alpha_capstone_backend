﻿using MediatR;
using Products.MicroService.Models;
using System.Collections.Generic;

namespace Products.MicroService.Queries
{
    public class GetProductByCategoryIdQuery : IRequest<IEnumerable<EcomProducts>>
    {
        public int CategoryId { get; set; }
    }
}
