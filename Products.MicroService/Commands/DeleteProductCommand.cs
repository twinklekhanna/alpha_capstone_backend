﻿using MediatR;
using Products.MicroService.Models;

namespace Products.MicroService.Commands
{
    public class DeleteProductCommand : IRequest<EcomProducts>
    {
        public int ProductId { get; set; }
    }
}
