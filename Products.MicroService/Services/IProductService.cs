﻿using Products.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Products.MicroService.Services
{
    public interface IProductService
    {
        Task<IEnumerable<EcomProducts>> AddProduct(EcomProducts product);
        Task<EcomProducts> DeleteProductById(int id);
        Task<IEnumerable<EcomProducts>> GetAllProducts();
        Task<EcomProducts> GetById(int id);
        Task<IEnumerable<EcomProducts>> UpdateProduct(EcomProducts product);
        Task<IEnumerable<EcomProducts>> GetProductByCategoryId(int categoryId);
    }
}