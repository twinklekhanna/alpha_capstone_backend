﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Products.MicroService.Commands;
using Products.MicroService.Models;
using Products.MicroService.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Products.MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ISender _send;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(ISender send, ILogger<ProductsController> logger)
        {
            _send = send;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<EcomProducts>> GetAllProducts()
        {
            return await _send.Send(new GetProductsQuery());
        }

        [HttpGet("getbyid/{id}")]
        public async Task<EcomProducts> GetById(int id)
        {
            return await _send.Send(new GetProductByIdQuery() { ProductId = id });
        }

        [HttpPost("add")]
        public async Task<IEnumerable<EcomProducts>> AddProduct([FromBody]EcomProducts product)
        {
            return await _send.Send(new AddProductCommand() { Product = product });
        }

        [HttpDelete("deletebyid/{id}")]
        public async Task<EcomProducts> DeleteProductById(int id)
        {
            return await _send.Send(new DeleteProductCommand() { ProductId = id });
        }

        [HttpPut("update")]
        public async Task<IEnumerable<EcomProducts>> UpdateProduct([FromBody]EcomProducts product)
        {
            return await _send.Send(new UpdateProductCommand() { Product = product });
        }

        [HttpGet("category/{categoryId}")]
        public async Task<IEnumerable<EcomProducts>> GetProsuctsByCategoryId(int categoryId)
        {
            return await _send.Send(new GetProductByCategoryIdQuery() { CategoryId = categoryId });
        }
    }
}
