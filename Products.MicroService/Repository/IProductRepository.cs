﻿using Products.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Products.MicroService.Repository
{
    public interface IProductRepository
    {
        Task<IEnumerable<EcomProducts>> GetAll();
    }
}